const express = require('express')
const app = express()

const port = 4000

app.get('/',(request,response) => {
    response.send('Hello World from Hazel')
})

app.post('/',(request,response) => {
    response.sendStatus(200)
})

app.listen(process.env.PORT || port, () => {
    console.log(`Now listening: ${process.env.PORT || port}`)
})